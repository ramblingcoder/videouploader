using DotNetOpenAuth.OAuth2;
using Google.Apis.Authentication.OAuth2;
using Google.Apis.Authentication.OAuth2.DotNetOpenAuth;
using Google.Apis.Samples.Helper;
using Google.Apis.Services;
using Google.Apis.Util;

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Net.Http;
using System.Net;
using Newtonsoft.Json;
using System.Reflection;
using System.Threading;
using Google.YouTube;

namespace VideoUploaderV3
{
	public class VideoUploader
	{
		/*
		[STAThread]
		public static void Main(string[] args)
		{
			//v3
			String metaInfo = JsonConvert.SerializeObject(
				new 
				{
					snippet = new {
						title = "Some Title",
						description = "Some Description",
						tags = new String[] {"tagA", "tagB"},
						categoryId = 20 //Gaming category
					},
					status = new {
						privacystatus = "private",
						embeddable = "True",
						license = "youtube"
					}
				});

			//V2
			metaInfo =
			"<?xml version='1.0'?>" +
			"<entry xmlns='http://www.w3.org/2005/Atom' xmlns:media='http://search.yahoo.com/mrss/' xmlns:yt='http://gdata.youtube.com/schemas/2007'>" +
				"<media:group>" +
					"<media:title type='plain'>Let's Play The Chronicles of Riddick - #09</media:title>" +
					"<media:description type='plain'>" +
						"I purchased this on steam and it doesn't appear to be available for purchase anymore. However, I've found an alternative on Amazon.\n\n" +
						"Game: http://www.amazon.com/dp/B001L18RIE\n\n"+
						"Although I did the recording, the game assets belong to the original owners."+
					"</media:description>" +
					"<media:category scheme='http://gdata.youtube.com/schemas/2007/categories.cat'>Games</media:category>" +
					"<media:keywords>let's play,the chronicles of riddick</media:keywords>" +
					"<media:license type='text/html' href='http://www.youtube.com/t/terms'>youtube</media:license>"+
					"<yt:private xmlns:yt='http://gdata.youtube.com/schemas/2007' />"+
				"</media:group>" +
			"</entry>";

			FileInfo videoFile = new FileInfo(@"C:\Pending Uploads\riddick09.mp4");

			new VideoUploader(metaInfo, videoFile);
		}
		 */

		#region Events
		public delegate void ProgressChangedEventHandler(int currentChunk, int totalChunks);
		public static event ProgressChangedEventHandler ProgressChanged;

		public delegate void StatusUpdatedEventHandler(String message);
		public static event StatusUpdatedEventHandler StatusUpdated;

		public delegate void ElapsedChangedEventHandler(TimeSpan elapsed);
		public static event ElapsedChangedEventHandler ElapsedChanged;

		public delegate void EstimatedChangedEventHandler(TimeSpan estimated);
		public static event EstimatedChangedEventHandler EstimatedChanged;
		#endregion

		#region members
		private String title = "Automatic";
		private String description = "Automatic";
		private String tags = "";
		private String category = "Games";
		private bool isPrivate = true;
		private FileInfo videoFile = null;

		private string storageName = String.Empty;
		private string storageKey = String.Empty;
		private string apikey = String.Empty;
		private string clientid = String.Empty;
		private string clientsecret = String.Empty;

		private int maxRetrys = 20;
		private int defaultDelayBetweenRetry = 1;

		private Uri resumableURI = null;
		private int chunkIndex = 0;
		private int chunkSize = 1024 * 1024; /*1MB*/

		private OAuth2Authenticator<NativeApplicationClient> auth;
		#endregion

		#region properties
		public String Title
		{
			get { return title; }
			set { title = value; }
		}
		public String Description
		{
			get { return description; }
			set { description = value; }
		}
		public String Tags
		{
			get { return tags; }
			set { tags = value; }
		}
		public String Category
		{
			get { return category; }
			set { category = value; }
		}
		public bool IsPrivate
		{
			get { return isPrivate; }
			set { isPrivate = value; }
		}
		public FileInfo VideoFile
		{
			get { return videoFile; }
			set { videoFile = value; }
		}

		public String StorageName
		{
			get { return storageName; }
			set { storageName = value; }
		}
		public String StorageKey
		{
			get { return storageKey; }
			set { storageKey = value; }
		}
		public String APIKey
		{
			get { return apikey; }
			set { apikey = value; }
		}
		public String ClientID
		{
			get { return clientid; }
			set { clientid = value; }
		}
		public String ClientSecret
		{
			get { return clientsecret; }
			set { clientsecret = value; }
		}

		public int MaxRetrys
		{
			get { return maxRetrys; }
			set { maxRetrys = value; }
		}
		public int DefaultDelayBetweenRetry
		{
			get { return defaultDelayBetweenRetry; }
			set { defaultDelayBetweenRetry = value; }
		}

		public Uri ResumableURI
		{
			get { return resumableURI; }
			set { resumableURI = value; }
		}
		public int ChunkIndex
		{
			get { return chunkIndex; }
			set { chunkIndex = value; }
		}
		public int ChunkSize
		{
			get { return chunkSize; }
			set { chunkSize = value; }
		}
		#endregion

		#region Authentication
		public void Authorize()
		{
			StatusUpdated.Invoke("Starting authorization.");

			if (String.IsNullOrEmpty(APIKey))
				throw new Exception("Missing 'API Key'.");
			if (String.IsNullOrEmpty(ClientID))
				throw new Exception("Missing 'Client ID'.");
			if (String.IsNullOrEmpty(ClientSecret))
				throw new Exception("Missing 'Client Secret'.");

			FullClientCredentials credentials = new FullClientCredentials()
			{
				ApiKey = apikey,
				ClientId = clientid,
				ClientSecret = clientsecret
			};

			NativeApplicationClient client = new NativeApplicationClient(GoogleAuthenticationServer.Description)
			{
				ClientIdentifier = credentials.ClientId,
				ClientSecret = credentials.ClientSecret
			};

			auth = new OAuth2Authenticator<NativeApplicationClient>(client, GetAuthorization);

			StatusUpdated.Invoke("Requesting user authorization.");
			client.RequestUserAuthorization();

			StatusUpdated.Invoke("Done with authorization.");
		}

		private IAuthorizationState GetAuthorization(NativeApplicationClient arg)
		{
			StatusUpdated.Invoke("Entered GetAuthorization.");

			IAuthorizationState state = null;
			try
			{
				state = AuthorizationMgr.GetCachedRefreshToken(storageName, storageKey);
				StatusUpdated.Invoke("Tried loading cached token.");
			}
			catch
			{ //Unable to get cached token, will need to request it again.
				StatusUpdated.Invoke("Unable to get cached refresh token. Reauthenticating.");
			}

			try
			{
				StatusUpdated.Invoke(String.Format("Refresh Token Successful? {0}", arg.RefreshToken(state)));
			}
			catch
			{ //Error loading the token because of a new storage info difference or invalid auth info.
				StatusUpdated.Invoke("Getting new authorization.");
				state = AuthorizationMgr.RequestNativeAuthorization(arg, "https://gdata.youtube.com");
				AuthorizationMgr.SetCachedRefreshToken(storageName, storageKey, state);
			}

			StatusUpdated.Invoke("Leaving GetAuthorization.");

			return state;
		}

		public void SignRequest(OAuth2Authenticator<NativeApplicationClient> Authenticator, HttpRequestMessage request)
		{
			var authorization = HttpRequestHeader.Authorization.ToString();
			var request2 = WebRequest.Create("http://www.hack.com") as HttpWebRequest;
			Authenticator.ApplyAuthenticationToRequest(request2);
			if (request2.Headers[authorization] != null)
			{
				request.Headers.Remove(authorization);
				request.Headers.Add(authorization, request2.Headers[authorization]);
			}
		}
		#endregion

		#region Uploading
		public void Upload()
		{
			StatusUpdated.Invoke("Entering Upload.");

			if (resumableURI != null)
				StatusUpdated.Invoke("Will attempt to resume video upload with Uri " + ResumableURI.AbsolutePath);
			if (chunkIndex != 0)
				StatusUpdated.Invoke("Will attempt to resume video upload at chunk " + ChunkIndex);
			StatusUpdated.Invoke("Chunk size is now " + ChunkSize + " bytes.");

			if (VideoFile == null)
				throw new Exception(String.Format("The video file was not set."));
			if (!VideoFile.Exists)
				throw new Exception(String.Format("The video file does not exist at {0}.", VideoFile.FullName));

			bool canUpload = true;
			if (ResumableURI == null)
			{
				try
				{
					StatusUpdated.Invoke("Attempting to upload meta information.");
					HttpResponseMessage startResponse = UploadMetaInformation(GenerateMetaInformation());
					//StatusUpdated.Invoke(startResponse);

					if (startResponse.StatusCode != HttpStatusCode.OK)
					{
						StatusUpdated.Invoke("Error Occurred [StatusCode]: " + startResponse.StatusCode);
						StatusUpdated.Invoke("Error Occurred [Message]: " + startResponse.RequestMessage);
						canUpload = false;
					}
					else
					{
						StatusUpdated.Invoke(String.Format("Retrieved upload location. {0}", startResponse.Headers.Location.AbsolutePath));
						ResumableURI = startResponse.Headers.Location;
					}
				}
				catch (Exception e)
				{
					StatusUpdated.Invoke("Error Occurred [Message]: " + e.Message);
					StatusUpdated.Invoke("Error Occurred [Stack]: " + e.StackTrace);
					canUpload = false;
				}
			}

			if (canUpload)
			{
				StatusUpdated.Invoke("Starting Upload.");

				StartUpload();
			}

			StatusUpdated.Invoke("Exiting Upload.");
		}

		private string GenerateMetaInformation()
		{
			return
			"<?xml version='1.0'?>" +
			"<entry xmlns='http://www.w3.org/2005/Atom' xmlns:media='http://search.yahoo.com/mrss/' xmlns:yt='http://gdata.youtube.com/schemas/2007'>" +
				"<media:group>" +
					(!String.IsNullOrEmpty(title) ? String.Format("<media:title type='plain'>{0}</media:title>",Title) : "") +
					(!String.IsNullOrEmpty(description) ? String.Format("<media:description type='plain'>{0}</media:description>",Description) : "") +
					(!String.IsNullOrEmpty(category) ? String.Format("<media:category scheme='http://gdata.youtube.com/schemas/2007/categories.cat'>{0}</media:category>",Category) : "") +
					(!String.IsNullOrEmpty(tags) ? String.Format("<media:keywords>{0}</media:keywords>",Tags) : "") +
					(isPrivate ? "<yt:private xmlns:yt='http://gdata.youtube.com/schemas/2007' />" : "") +
				"</media:group>" +
			"</entry>";
		}

		private HttpResponseMessage UploadMetaInformation(String metaInfo)
		{
			//API V2 version
			StringContent content = new StringContent(metaInfo, Encoding.UTF8, "application/atom+xml");

			HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Post, "http://uploads.gdata.youtube.com/resumable/feeds/api/users/default/uploads");
			SignRequest(auth, request);

			request.Content = content;

			request.Headers.Add("GData-Version", "2");
			request.Headers.Add("X-GData-Key", "key=" + apikey);
			request.Headers.Add("Slug", "" + VideoFile.Name);

			StatusUpdated.Invoke("Sending request for resumable URI.");
			//StatusUpdated.Invoke(request);

			HttpClient httpClient = new HttpClient();
			return httpClient.SendAsync
				(
					request
				).ContinueWith(httpResponseMessage =>
				{
					return httpResponseMessage.Result;
				}).Result;
		}

		private void StartUpload()
		{
			Stopwatch elapsedTimer = new Stopwatch();
			int currentDelayBetweenRetrys = DefaultDelayBetweenRetry;
			int retryAttempt = 0;

			elapsedTimer.Start();

			int totalChunks = (videoFile.Length % chunkSize == 0 ? (int)(videoFile.Length / chunkSize) : (int)(videoFile.Length / chunkSize) + 1);
			StatusUpdated.Invoke(String.Format("{0} chunks to upload.", totalChunks));

			for (int chunkIndex = ChunkIndex; chunkIndex < totalChunks; )
			{
				EstimatedChanged.Invoke(new TimeSpan(0, 0, (int)((elapsedTimer.Elapsed.TotalSeconds / (double)chunkIndex) * ((double)totalChunks - (double)chunkIndex))));
				ElapsedChanged.Invoke(elapsedTimer.Elapsed);

				HttpResponseMessage uploadResponse = null;
				try
				{
					StatusUpdated.Invoke("Uploading chunk " + (chunkIndex + 1) + " of " + totalChunks + ".");
					uploadResponse = UploadVideoChunk(chunkIndex);

					ProgressChanged.Invoke(chunkIndex, totalChunks - 1);

					StatusUpdated.Invoke("Received response for " + (chunkIndex + 1));
					//StatusUpdated.Invoke(uploadResponse);

					currentDelayBetweenRetrys = DefaultDelayBetweenRetry;
					retryAttempt = 0;
				}
				catch (Exception)
				{
					retryAttempt = retryAttempt + 1;

					StatusUpdated.Invoke(String.Format("Exception occurred. Retry {0} in {1} seconds", retryAttempt, currentDelayBetweenRetrys));
					//StatusUpdated.Invoke(e);

					Thread.Sleep(1000 * currentDelayBetweenRetrys);
					currentDelayBetweenRetrys = currentDelayBetweenRetrys * 2; //exponential growth

					if (retryAttempt <= MaxRetrys)
					{
						continue; //retry
					}
					else
					{
						StatusUpdated.Invoke("Exceeded retry count. Ending attempts.");
						break;
					}
				}

				switch (uploadResponse.StatusCode)
				{
					case (HttpStatusCode)308: //Good response, continue
						chunkIndex += 1;
						break;
					case (HttpStatusCode)200: //Upload Done
						StatusUpdated.Invoke("Upload done (200). ");
						chunkIndex += 1; //Should break the loop
						break;
					case (HttpStatusCode)201: //Upload Done
						StatusUpdated.Invoke("Upload done (201). ");
						chunkIndex += 1; //Should break the loop
						break;
					case (HttpStatusCode)500: //Recoverable, retry
						StatusUpdated.Invoke("Resumable Failure 500. Retrying " + chunkIndex);
						break;
					case (HttpStatusCode)502: //Recoverable, retry
						StatusUpdated.Invoke("Resumable Failure 502. Retrying " + chunkIndex);
						break;
					case (HttpStatusCode)503: //Recoverable, retry
						StatusUpdated.Invoke("Resumable Failure 503. Retrying " + chunkIndex);
						break;
					case (HttpStatusCode)504: //Recoverable, retry
						StatusUpdated.Invoke("Resumable Failure 504. Retrying " + chunkIndex);
						break;
					default:
						StatusUpdated.Invoke("Fatal Failure " + (int)uploadResponse.StatusCode + " at chunk " + chunkIndex + ". Stopping.");
						//StatusUpdated.Invoke(uploadResponse);
						chunkIndex = totalChunks + 1; //Unrecoverable, break loop.
						break;
				}
			}

			elapsedTimer.Stop();
		}

		private HttpResponseMessage UploadVideoChunk(int chunkIndex)
		{
			int contentLength = ChunkSizeForIndex(videoFile.Length, chunkIndex, chunkSize);

			byte[] loadedContent = new byte[contentLength];

			using (FileStream fileStream = new FileStream(videoFile.FullName, FileMode.Open))
			{
				fileStream.Position = (long)chunkSize * (long)chunkIndex;
				fileStream.Read(loadedContent, 0, contentLength);
			}

			ByteArrayContent content = new ByteArrayContent(loadedContent);
			content.Headers.Add("Content-Range", String.Format("bytes {0}-{1}/{2}", (long)chunkIndex * (long)chunkSize, (long)chunkIndex * (long)chunkSize + (long)contentLength - 1L, videoFile.Length));

			HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Put, ResumableURI)
			{
				Content = content
			};

			StatusUpdated.Invoke("Sending request for chunk upload. Chunk " + (chunkIndex + 1));
			//StatusUpdated.Invoke(request);

			HttpClient httpClient = new HttpClient();
			return httpClient.SendAsync
				(
					request
				).ContinueWith(httpResponseMessage =>
				{
					return httpResponseMessage.Result;
				}).Result;
		}

		private int ChunkSizeForIndex(long fileLength, int chunkIndex, int chunkSize)
		{
			if (((long)chunkIndex + 1L) * (long)chunkSize < fileLength) //start of next chunk is still within the total length so return full chunk size
				return chunkSize;
			else
				return (int)(fileLength % chunkSize); //Return the remainder if the condition of the above is false because we've excceded the length of the file
		}
		#endregion
	}
}
