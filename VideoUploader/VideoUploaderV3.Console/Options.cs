﻿using CommandLine;
using CommandLine.Text;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VideoUploaderV3.Console
{
	class Options
	{
		[Option('a', "apikey", Required = true, HelpText = "The api key.")]
		public string APIKey { get; set; }

		[Option('c', "clientid", Required = true, HelpText = "The client id.")]
		public string ClientID { get; set; }

		[Option('s', "clientsecret", Required = true, HelpText = "The client secret.")]
		public string ClientSecret { get; set; }

		[Option('n', "storagename", Required = false, HelpText = "The storage name.")]
		public string StorageName { get; set; }

		[Option('k', "storagekey", Required = false, HelpText = "The storage key.")]
		public string StorageKey { get; set; }

		[Option('v', "videofilepath", Required = false, HelpText = "The video to upload.")]
		public string VideoFilePath { get; set; }

		[Option('p', "videofolderpath", Required = false, HelpText = "The the folder containing videos to upload.")]
		public string VideoFolderPath { get; set; }


		[ParserState]
		public IParserState LastPareserState { get; set; }

		[HelpOption]
		public string GetUsage()
		{
			return HelpText.AutoBuild(this, (HelpText current) => HelpText.DefaultParsingErrorsHandler(this, current));
		}
	}
}
