﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VideoUploaderV3.Console
{
	class VideoUploaderConsole
	{
		static void Main(string[] args)
		{
			Options options = new Options();
			if (CommandLine.Parser.Default.ParseArguments(args, options))
			{
				new VideoUploaderConsole(options);
			}
		}

		public VideoUploaderConsole(Options options)
		{
			VideoUploader.ElapsedChanged += ElapsedChanged;
			VideoUploader.EstimatedChanged += EstimatedChanged;
			VideoUploader.ProgressChanged += ProgressChanged;
			VideoUploader.StatusUpdated += StatusChanged;

			List<String> videos = new List<string>();

			if (!String.IsNullOrEmpty(options.VideoFolderPath))
			{
				GetListOfFiles(videos, options.VideoFolderPath);
			}

			if (!String.IsNullOrEmpty(options.VideoFilePath))
			{
				videos.Add(options.VideoFilePath);
			}

			foreach (String video in videos)
			{
				FileInfo videoFile = new FileInfo(video);

				VideoUploader uploader = new VideoUploader()
				{
					VideoFile = videoFile,
					Title = videoFile.Name,
					APIKey = options.APIKey,
					ClientID = options.ClientID,
					ClientSecret = options.ClientSecret,
					StorageName = options.StorageName ?? "828173F5-02EE-4AD0-B142-4D781AECBDBF",
					StorageKey = options.StorageKey ?? "2ABF7618-4C96-4753-976F-2E6C186DDF25",
				};
				uploader.Authorize();
				uploader.Upload();

				videoFile.MoveTo(videoFile.FullName+".done");
			}
		}

		private static void GetListOfFiles(List<String> videos, String videoFolderPath)
		{
			DirectoryInfo info = new DirectoryInfo(videoFolderPath);

			foreach (FileInfo file in info.GetFiles())
			{
				if (file.FullName.ToLower().EndsWith(".avi") || file.FullName.ToLower().EndsWith(".mp4"))
					videos.Add(file.FullName);
			}

			foreach (DirectoryInfo directory in info.GetDirectories())
			{
				GetListOfFiles(videos, directory.FullName);
			}
		}

		private void StatusChanged(string message)
		{
			System.Console.WriteLine("Status: " + message);
		}

		private void ProgressChanged(int currentChunk, int totalChunks)
		{
			System.Console.WriteLine("Progress: Chunk " + currentChunk + " of " + totalChunks);
		}

		private void EstimatedChanged(TimeSpan estimated)
		{
			if (estimated.Ticks < 0)
				return;

			System.Console.WriteLine(String.Format("Estimated: {0} day{1} {2:D2}:{3:D2}:{4:D2}", estimated.Days, (estimated.Days > 1 || estimated.Days == 0 ? "s" : ""), estimated.Hours, estimated.Minutes, estimated.Seconds));
		}

		private void ElapsedChanged(TimeSpan elapsed)
		{
			System.Console.WriteLine(String.Format("Elapsed: {0} day{1} {2:D2}:{3:D2}:{4:D2}", elapsed.Days, (elapsed.Days > 1 || elapsed.Days == 0 ? "s" : ""), elapsed.Hours, elapsed.Minutes, elapsed.Seconds));
		}

		private string BuildMetaData()
		{
			String MetaData =
			"<?xml version='1.0'?>" +
			"<entry xmlns='http://www.w3.org/2005/Atom' xmlns:media='http://search.yahoo.com/mrss/' xmlns:yt='http://gdata.youtube.com/schemas/2007'>" +
				"<media:group>" +
				"<media:title type='plain'>AutomaticVideoUpload</media:title>"+
				"<media:description type='plain'>Uploaded "+DateTime.Now+"</media:description>"+
				"<media:category scheme='http://gdata.youtube.com/schemas/2007/categories.cat'>Games</media:category>"+
				"<yt:private xmlns:yt='http://gdata.youtube.com/schemas/2007' />"+
				"</media:group>" +
			"</entry>";

			return MetaData;
		}
	}
}
